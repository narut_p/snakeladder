package com.ske.snakebaddesign.models;

/**
 * Created by Net on 3/16/2016.
 */
public class DieCup {
    private Die die;

    public DieCup() {
        die = new Die();
    }

    public int roll() {
        return die.roll();
    }
}
