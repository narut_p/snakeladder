package com.ske.snakebaddesign.models;

import android.util.Log;

/**
 * Created by Net on 3/16/2016.
 */
public class Piece {
    private int position;

    public Piece(int position) {
        this.position = position;
    }

    public int getPosition() {
        return this.position;
    }

    public void setPosition(int position) {

        this.position = position;
    }

    public void reset() {
        this.position = 0;
    }
}
