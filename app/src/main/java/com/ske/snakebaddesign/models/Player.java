package com.ske.snakebaddesign.models;

import android.util.Log;

/**
 * Created by Net on 3/16/2016.
 */
public class Player {
    private int numPly;
    private Piece piece;

    public Player(int numPly) {
        this.numPly = numPly;
        piece = new Piece(0);
    }

    public int getPosition() {
        return piece.getPosition();
    }

    public void setPosition(int position) {

        piece.setPosition(position);
    }

    public int getNumPly() {
        return this.numPly;
    }
}
