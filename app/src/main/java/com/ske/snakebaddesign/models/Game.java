package com.ske.snakebaddesign.models;

import android.util.Log;

import com.ske.snakebaddesign.guis.BoardView;

import java.util.Observable;

/**
 * Created by Net on 3/16/2016.
 */
public class Game extends Observable {

    private int i, j;
    private DieCup diecup;
    private int boardSize;
    private int turn;
    private Player player1;
    private Player player2;

    public Game() {
        player1 = new Player(0);
        player2 = new Player(1);
        diecup = new DieCup();
        turn = 0;
        boardSize = 6;
    }

    public void setIJ(int i, int j) {
        this.i = i;
        this.j = j;
    }

    public void rolling(int temp) {
        //get a number of the dice
        if (turn % 2 == 0) {
            player1.setPosition(adjustPosition(player1.getPosition(), temp));
            if (player1.getPosition() == 21) {
                player1.setPosition(adjustPosition(player1.getPosition(), 7));
            }
            setChanged();
            notifyObservers(player1);
        } else {
            player2.setPosition(adjustPosition(player2.getPosition(), temp));
            if (player2.getPosition() == 21) {
                player2.setPosition(adjustPosition(player2.getPosition(), 7));
            }
            setChanged();
            notifyObservers(player2);
        }

        turn++;
    }

    private int adjustPosition(int current, int distance) {
        current = current + distance;
        int maxSquare = boardSize * boardSize - 1;
        if (current > maxSquare) {
            current = maxSquare - (current - maxSquare);
        }
        return current;
    }

    public void reset() {
        turn = 0;
        player1.setPosition(0);
        player2.setPosition(0);
        boardSize = 6;
    }

    public DieCup getDieCup() {
        return this.diecup;
    }

    public String getGameStatus() {
        String temp = "";
        if (player1.getPosition() == boardSize * boardSize - 1) {
            temp = "Player 1 won!";
        } else if (player2.getPosition() == boardSize * boardSize - 1) {
            temp = "Player 2 won!";
        }
        return temp;
    }
}
