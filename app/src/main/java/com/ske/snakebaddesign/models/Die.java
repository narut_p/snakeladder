package com.ske.snakebaddesign.models;

import java.util.Random;

/**
 * Created by Net on 3/16/2016.
 */
public class Die {

    public int roll() {
        int random = new Random().nextInt(6) + 1;
        return random;
    }
}
